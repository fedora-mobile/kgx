%global gtk3_version 3.24
%global glib_version 2.58
%global vte_version 0.57
%global libhandy_version 1.0.0
%global gettext_version 0.19.8
%global app_id org.gnome.zbrown.KingsCross

Name: kgx
Version: 0.2.1
Release: 2%{?dist}
Summary: A minimal terminal for GNOME

License: GPLv3+
URL: https://gitlab.gnome.org/ZanderBrown/kgx
Source0: https://gitlab.gnome.org/ZanderBrown/%{name}/-/archive/%{version}/kgx-%{version}.tar.gz

BuildRequires: pkgconfig(gtk+-3.0) >= %{gtk3_version}
BuildRequires: pkgconfig(gio-2.0) >= %{glib_version}
BuildRequires: pkgconfig(libhandy-1) >= %{libhandy_version}
BuildRequires: pkgconfig(vte-2.91) >= %{vte_version}
BuildRequires: pkgconfig(libgtop-2.0)
BuildRequires: desktop-file-utils
BuildRequires: gettext >= %{gettext_version}
BuildRequires: clang
BuildRequires: meson
BuildRequires: ninja-build
BuildRequires: desktop-file-utils
BuildRequires: libappstream-glib

Requires: hicolor-icon-theme

%description
KGX is supposed to be a simple terminal emulator for the average user to carry
out simple cli tasks and aims to be a 'core' app for GNOME/Phosh.

%prep
%setup -q
desktop-file-validate %{buildroot}%{_datadir}/applications/%{app_id}.desktop
appstream-util validate-relax --nonet %{buildroot}%{_metainfodir}/%{app_id}.appdata.xml

%build
%meson -Dgtop=true
%meson_build

%install
%meson_install
%find_lang %{name} --with-gnome

%files -f %{name}.lang
%license COPYING
%doc README.md
%{_bindir}/%{name}
%{_datadir}/dbus-1/services/%{app_id}.service
%{_datadir}/glib-2.0/schemas/%{app_id}.gschema.xml
%{_datadir}/appdata/%{app_id}.appdata.xml
%{_datadir}/applications/%{app_id}.desktop
%{_datadir}/icons/hicolor/*/apps/%{app_id}.svg
%{_datadir}/icons/hicolor/symbolic/apps/%{app_id}-symbolic.svg
%{_datadir}/icons/hicolor/*/apps/%{app_id}.Generic.svg
%{_datadir}/icons/hicolor/symbolic/apps/%{app_id}.Generic-symbolic.svg

%changelog
* Sat Mar 06 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 0.2.1-2
- rebuild for newer libhandy

* Thu Oct 24 2019 Christopher Davis <christopherdavis@gnome.org> - 0.2.1-1
- Initial RPM
